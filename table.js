document.addEventListener('DOMContentLoaded', () => {
    // -- your code here, after the DOM has been fully loaded

    let articles = [
        {
        price : 2 , 
        quantity : 3 ,
        name : "Salade"
        },
        {
        price : 3 ,
        quantity : 2 ,
        name : "Pâtes"
        },
        {
        price : 3 ,
        quantity : 10 ,
        name : "Coca-Cola"
        },
        {
        price : 2 ,
        quantity : 5 ,
        name : "Bn"
        },
        {
        price : 3 ,
        quantity : 4 ,
        name : "Jambon"
        },
        {
        price : 6 ,
        quantity : 1 ,
        name : "Steak"
        }
    ];

    function generateTableHead(table , data)
    {
        let thead = table.createTHead();
        let row = thead.insertRow();
       
      
        for(let key of data )
        {
            let th = document.createElement("th");
            th.setAttribute( "scope" , "col");
            let text = document.createTextNode(key );
            th.appendChild(text) ;
            row.appendChild(th) ;
        }
    }


    function generateTable(table , data)
    {
        for (let element of data )
        {
            let row = table.insertRow();
            row.setAttribute( "class" , "product");
            for (key in element )
            {
                let cell = row.insertCell();
                let text =document.createTextNode(element[key]);
                cell.appendChild(text)
            }
        }
    }


    let table = document.querySelector("table");
    let data = Object.keys(articles[0]);
    generateTableHead(table , data);
    generateTable(table ,articles )
})